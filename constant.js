const ROLE = {
  ADMIN: 'ADMIN',
  LOCALADMIN: 'LOCAL_ADMIN',
  APPROVER: 'APPROVER',
  USER: 'USER'
}

module.exports = {
  ROLE
}
