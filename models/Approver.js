const mongoose = require('mongoose')
const { Schema } = mongoose
const approverSchema = Schema({
  name: String,
  order1: { type: Schema.Types.ObjectId, ref: 'User' },
  order2: { type: Schema.Types.ObjectId, ref: 'User' }
})
module.exports = mongoose.model('Approver', approverSchema)
