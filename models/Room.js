const mongoose = require('mongoose')
const { Schema } = mongoose
const roomSchema = Schema({
  name: String,
  desc: String,
  capacity: Number,
  building: { type: Schema.Types.ObjectId, ref: 'Building' },
  institution: { type: Schema.Types.ObjectId, ref: 'Institution' },
  approver: { type: Schema.Types.ObjectId, ref: 'Approver' }

})

module.exports = mongoose.model('Room', roomSchema)
