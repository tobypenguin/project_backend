const mongoose = require('mongoose')
const { Schema } = mongoose
const approvelistSchema = Schema({
  booking: { type: Schema.Types.ObjectId, ref: 'Booking' },
  status: String,
  user_id: { type: Schema.Types.ObjectId, ref: 'User' },
  current_order: Number
})
module.exports = mongoose.model('Approverlist', approvelistSchema)
