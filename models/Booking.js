const mongoose = require('mongoose')

const { Schema } = mongoose
const bookingSchema = Schema({
  name: String,
  email: String,
  desc: String,
  building: { type: Schema.Types.ObjectId, ref: 'Building' },
  room: { type: Schema.Types.ObjectId, ref: 'Room' },
  startDate: Date,
  endDate: Date

},
{
  timestamps: true
})

module.exports = mongoose.model('Booking', bookingSchema)
