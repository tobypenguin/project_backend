const express = require('express')
const router = express.Router()
const Institution = require('../models/Institution.js')

const getInstitutions = async (req, res, next) => {
  try {
    const institutions = await Institution.find({}).exec()
    res.json(institutions)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const getInstitution = async function (req, res, next) {
  const id = req.params.id
  console.log(id)
  try {
    const institution = await Institution.findById(id).exec()
    if (institution === null) {
      return res.status(404).json({
        message: 'Institution not found.'
      })
    }
    res.json(institution)
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
}

const addInstitutions = async (req, res, next) => {
  const newInstitution = new Institution({
    name: req.body.name,
    rolesinstitution: req.body.rolesinstitution
  })
  try {
    await newInstitution.save()
    res.status(201).json(newInstitution)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const updateInstitution = async (req, res, next) => {
  const institutionId = req.params.id
  try {
    const institution = await Institution.findById(institutionId)
    institution.name = req.body.name
    await institution.save()
    return res.status(200).json(institution)
  } catch (err) {
    return res.status(404).send({ messag: err.messag })
  }
}

const deleteInstitution = async (req, res, next) => {
  const institutionId = req.params.id
  try {
    await Institution.findByIdAndDelete(institutionId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }

  // const index = institutions.findIndex(function (item) {
  //   return item.id === institutionId
  // })
  // if (index >= 0) {
  //   institutions.splice(index, 1)
  //   res.status(204).send()
  // } else {
  //   res.status(404).json({
  //     code: 404,
  //     msg: 'No institution id ' + req.params.id
  //   })
  // }
}

router.get('/', getInstitutions) // GET Institutions
router.get('/:id', getInstitution) // GET One Institution
router.post('/', addInstitutions) // Add New Institution
router.put('/:id', updateInstitution) // Add New Institution
router.delete('/:id', deleteInstitution) // Delete Institution

module.exports = router
