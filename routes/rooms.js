const express = require('express')

const router = express.Router()
const Room = require('../models/Room')

const getRooms = async function (req, res, next) {
  try {
    const rooms = await Room.find({}).populate({
      path: 'building'
    }).populate({ path: 'institution' }).populate({ path: 'approver', populate: { path: 'order1' } }).exec()
    res.json(rooms)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}
const addRoom = async function (req, res, next) {
  try {
    const room = new Room({
      name: req.body.name,
      desc: req.body.desc,
      capacity: req.body.capacity,
      building: req.body.building,
      institution: req.body.institution,
      approver: req.body.approver

    })
    await room.save()
    res.status(201).json(room)
  } catch (e) {
    res.status(409).json({ message: e.message })
  }
}

const getRoom = async function (req, res, next) {
  const room = await Room.findById(req.params.id)
  res.status(200).json(room)
}
const updateRoom = async function (req, res, next) {
  const room = await Room.findByIdAndUpdate(req.params.id, req.body, { new: true })
  if (room !== null) {
    res.status(200).json(room)
  } else {
    res.status(404).json({ message: 'Unable todate ' + req.params.id })
  }
}
const updatePartialRoom = async function (req, res, next) {
  const room = await Room.findByIdAndUpdate(req.params.id, req.body, { new: true })
  if (room !== null) {
    res.status(200).json(room)
  } else {
    res.status(404).json({ message: 'Unable todate ' + req.params.id })
  }
}

const deleteRoom = async function (req, res, next) {
  await Room.findByIdAndDelete(req.params.id)
}
router.get('/', getRooms)
router.post('/', addRoom)
router.get('/:id', getRoom)
router.put('/:id', updateRoom)
router.patch('/:id', updatePartialRoom)
router.delete('/:id', deleteRoom)

module.exports = router
