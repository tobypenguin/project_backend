const express = require('express')
const router = express.Router()
const User = require('../models/User')

const getUserApprover = async function (req, res, next) {
  try {
    const users = await User.find({ roles: 'APPROVER' }).exec()
    res.json(users)
  } catch (e) {
    return res.status(500).send({
      message: e.message
    })
  }
}

router.get('/', getUserApprover) // GET Users

module.exports = router
