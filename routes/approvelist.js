const express = require('express')
const router = express.Router()
const Approvelist = require('../models/Approvelist')
const Approver = require('../models/Approver')
const Booking = require('../models/Booking')
const Room = require('../models/Room')

const getApprovelistid = async (req, res, next) => {
  const id = req.params.id
  const ObjectId = require('mongoose').Types.ObjectId
  try {
    const approvelistid = await Approvelist.find({ user_id: new ObjectId(id) }).populate({ path: 'user_id' }).populate({ path: 'booking' }).exec()
    if (approvelistid === null) {
      return res.status(404).json({
        message: 'approvelistid not found'
      })
    }
    // console.log(approvelistid)
    res.json(approvelistid)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
}

const getApprovelists = async (req, res, next) => {
  try {
    const approvelists = await Approvelist.find({}).populate({ path: 'user_id' }).populate({ path: 'booking', populate: { path: 'room' } }).exec()
    res.json(approvelists)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const addApprovelist = async function (req, res, next) {
  try {
    // console.log(req.body.booking)
    const approvelist = new Approvelist({
      booking: req.body.booking,
      status: 'รออนุมัติ',
      user_id: req.body.user_id,
      current_order: 1
    })
    await approvelist.save()
    res.status(201).json(approvelist)
  } catch (e) {
    console.log(e)
  }
}

const updateApprovelist = async function (req, res, next) {
  const approvelist = await Approvelist.findByIdAndUpdate(req.params.id, req.body, { new: true })
  if (approvelist !== null) {
    res.status(200).json(approvelist)
  } else {
    res.status(404).json({ message: 'Unable todate ' + req.params.id })
  }
}

const deleteApprovelist = async function (req, res, next) {
  await Approvelist.findByIdAndDelete(req.params.id)
}

const approved = async function (req, res, next) {
  const id = req.body._id
  try {
    const approveList = await Approvelist.findById(id).exec()
    console.log(approveList.booking + 'book')
    const booking = await Booking.findById(approveList.booking)
    console.log(booking.room + 'room')
    const room = await Room.findById(booking.room)
    console.log(room.approver + 'approver')
    const approve = await Approver.findById(room.approver)
    approveList.current_order++
    if (approveList.current_order === 2) {
      approveList.user_id = approve.order2
      approveList.status = 'รอการอนุมัติ'
    } else if (approveList.current_order === 3) {
      approveList.user_id = null
      approveList.status = 'อนุมัติ'
    }
    await approveList.save()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

const notApproved = async function (req, res, next) {
  const id = req.body._id
  try {
    const approveList = await Approvelist.findById(id).exec()
    approveList.user_id = null
    approveList.status = 'ไม่อนุมัติ'
    if (approveList.status === 'ไม่อนุมัติ') {
      const booking = await Booking.findByIdAndDelete(approveList.booking)
      const deleteApprovelist = await Approvelist.findByIdAndDelete(approveList._id)
      console.log(booking.room + 'room')
      console.log(deleteApprovelist)
    }
    await approveList.save()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

router.get('/', getApprovelists)
router.post('/', addApprovelist)
router.put('/:id', updateApprovelist)
router.delete('/:id', deleteApprovelist)
router.post('/approved', approved)
router.post('/cancel', notApproved)
router.get('/Approvelist/:id', getApprovelistid)

module.exports = router
