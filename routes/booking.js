const express = require('express')
const router = express.Router()
const Booking = require('../models/Booking')

const getBooking = async function (req, res, next) {
  try {
    const buildings = await Booking.find({}).populate({ path: 'room', populate: { path: 'approver' } }).exec()
    console.log()
    res.json(buildings)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}
const getBookingTime = async function (req, res, next) {
  try {
    const id = req.params.id
    const ObjectId = require('mongoose').Types.ObjectId
    const bookings = await Booking.find({ room: new ObjectId(id) }).exec()
    console.log(bookings)
    res.json(bookings)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}
const addBooking = async function (req, res, next) {
  try {
    const booking = new Booking({
      name: req.body.name,
      email: req.body.email,
      desc: req.body.desc,
      building: req.body.building,
      room: req.body.room,
      startDate: new Date(req.body.startDate + ' ' + req.body.startTime),
      endDate: new Date(req.body.endDate + ' ' + req.body.endTime)
    })
    await booking.save()
    res.status(201).json(booking)
  } catch (e) {
    res.status(409).json({ message: e.message })
  }
}
const deleteBooking = async function (req, res, next) {
  await Booking.findByIdAndDelete(req.params.id)
}

router.get('/', getBooking)
router.get('/BookingTime/:id', getBookingTime)
router.delete('/:id', deleteBooking)
router.post('/', addBooking)
module.exports = router
