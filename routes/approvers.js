const express = require('express')

const router = express.Router()
const Approver = require('../models/Approver')

const getApprovers = async function (req, res, next) {
  try {
    const approver = await Approver.find({}).populate({ path: 'order1' }).populate({ path: 'order2' }).exec()
    res.json(approver)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}
const getApprover = async function (req, res, next) {
  const building = await Approver.findById(req.params.id)
  res.status(200).json(building)
}
const addAprover = async function (req, res, next) {
  try {
    const approver = new Approver({
      name: req.body.name,
      order1: req.body.order1,
      order2: req.body.order2
    })
    await approver.save()
    res.status(201).json(approver)
  } catch (e) {
    res.status(409).json({ message: e.message })
  }
}
const updateApprover = async function (req, res, next) {
  const approver = await Approver.findByIdAndUpdate(req.params.id, req.body, { new: true })
  if (approver !== null) {
    res.status(200).json(approver)
  } else {
    res.status(404).json({ message: 'Unable todate ' + req.params.id })
  }
}
const deleteApprover = async function (req, res, next) {
  await Approver.findByIdAndDelete(req.params.id)
}

router.get('/', getApprovers)
router.get('/:id', getApprover)
router.post('/', addAprover)
router.put('/:id', updateApprover)
router.delete('/:id', deleteApprover)

module.exports = router
