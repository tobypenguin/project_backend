const mongoose = require('mongoose')
const User = require('../models/User')
const { ROLE } = require('../constant')
mongoose.connect('mongodb://localhost:27017/example')
async function clearUser () {
  await User.deleteMany({})
}

async function main () {
  await clearUser()

  const user = new User({ username: 'user@mail.com', password: '123', name: 'Boyarin', surname: 'Sukjarune', roles: [ROLE.USER] })
  user.save()
  const admin = new User({ username: 'admin@mail.com', password: '123', name: 'Jarunee', surname: 'Sakjaruen', roles: [ROLE.ADMIN, ROLE.USER] })
  admin.save()
  const localAdmin = new User({ username: 'localadmin@mail.com', password: '123', name: 'Pubodin', surname: 'Sukjarune', roles: [ROLE.LOCALADMIN, ROLE.USER] })
  localAdmin.save()
  const approver = new User({ username: 'approver@mail.com', password: '123', name: 'Obibo', surname: 'Kenoboy', roles: [ROLE.APPROVER, ROLE.USER] })
  approver.save()
}

main().then(function () {
  console.log('Finish')
})
