const mongoose = require('mongoose')
const Approver = require('../models/Approver')
mongoose.connect('mongodb://localhost:27017/example')

async function clear () {
  await Approver.deleteMany({})
}

async function main () {
  await clear()
  Approver.insertMany([{
    name: 'ผู้พิจารณา 1',
    order: ['6256a22c480ed8d22f5851cb']

  }
  ])
}
main().then(function () {
  console.log('Finish')
})
