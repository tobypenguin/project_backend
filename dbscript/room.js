const mongoose = require('mongoose')
const Room = require('../models/Room')
mongoose.connect('mongodb://localhost:27017/example')

async function clear () {
  await Room.deleteMany({})
}

async function main () {
  await clear()
  Room.insertMany([{
    name: 'IF-3C01',
    desc: 'จอสำหรับการสอน 1 จอ, เก้าอี้ 20 ตัว, คอมพิวเตอร์ 20 เครื่อง, ไมค์โครโฟน 1 ตัว, ลำโพง 2 ตัว',
    capacity: 20,
    building: '62541b8159975d54fc0f420f',
    institution: '6257ec5e6edd19a7b2377ab2',
    approver: '6256a2e5972cbe4994ddd633'
  },
  {
    name: 'IF-3C02',
    desc: 'จอสำหรับการสอน 1 จอ, เก้าอี้ 20 ตัว, คอมพิวเตอร์ 20 เครื่อง, ไมค์โครโฟน 1 ตัว, ลำโพง 2 ตัว',
    capacity: 20,
    building: '62541b8159975d54fc0f420f',
    institution: '6257ec5e6edd19a7b2377ab2',
    approver: '6256a2e5972cbe4994ddd633'
  }
  ])
}
main().then(function () {
  console.log('Finish')
})
