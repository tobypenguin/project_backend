const mongoose = require('mongoose')
const Institution = require('../models/institution.js')

mongoose.connect('mongodb://localhost:27017/example')
async function clearInstitution () {
  await Institution.deleteMany({})
}

async function main () {
  await clearInstitution()

  const Informatics = new Institution({ name: 'คณะวิทยาการสารสนเทศ' })
  Informatics.save()
  const Science = new Institution({ name: 'คณะวิทยาศาสตร์' })
  Science.save()
}

main().then(function () {
  console.log('Finish')
})
