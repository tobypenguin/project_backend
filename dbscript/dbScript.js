const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/example')
const Room = require('../models/Room')
const Building = require('../models/Building')
const User = require('../models/User')
const Approver = require('../models/Approver')
const Institution = require('../models/institution')
const { ROLE } = require('../constant')
const booking = require('../models/Booking')
const approvelist = require('../models/Approvelist')

async function clearDb () {
  await Room.deleteMany({})
  await Building.deleteMany({})
  await User.deleteMany({})
  await Approver.deleteMany({})
  await Institution.deleteMany({})
  await booking.deleteMany({})
  await approvelist.deleteMany({})
}

async function main () {
  await clearDb()
  const building1 = new Building({
    name: 'อาคารคณะวิทยาการสารสนเทศ',
    code: 'IF',
    level: 11
  })
  const building2 = new Building({
    name: 'อาคารสิรินธร',
    code: 'SD',
    level: 5
  })
  const Inst1 = new Institution({ name: 'คณะวิทยาการสารสนเทศ' })
  const Inst2 = new Institution({ name: 'คณะวิทยาศาสตร์' })
  const user = new User({ username: 'user@mail.com', password: '123', name: 'Boyarin', surname: 'Sukjarune', roles: [ROLE.USER] })
  const admin = new User({ username: 'admin@mail.com', password: '123', name: 'Jarunee', surname: 'Sakjaruen', roles: [ROLE.ADMIN, ROLE.USER] })
  const localAdmin = new User({ username: 'localadmin@mail.com', password: '123', name: 'Pubodin', surname: 'Sukjarune', roles: [ROLE.LOCALADMIN, ROLE.USER] })
  const approver1 = new User({ username: 'approver@mail.com', password: '123', name: 'Mankhong', surname: 'Limprapaipong', roles: [ROLE.APPROVER, ROLE.USER] })
  const approver2 = new User({ username: 'approver2@mail.com', password: '123', name: 'Suwan', surname: 'Srisompong', roles: [ROLE.APPROVER, ROLE.USER] })
  const approver3 = new User({ username: 'kitti@mail.com', password: '123', name: 'kitti', surname: 'Kredtong', roles: [ROLE.APPROVER, ROLE.USER] })
  const approver4 = new User({ username: 'Thana@mail.com', password: '123', name: 'Thana', surname: 'Phakdi', roles: [ROLE.APPROVER, ROLE.USER] })
  const approve1 = new Approver({ name: 'ผู้พิจารณา 1', order1: approver1, order2: approver2 })
  const approve2 = new Approver({ name: 'ผู้พิจารณา 2', order1: approver3, order2: approver4 })
  const room1 = new Room({
    name: 'IF-3C01',
    desc: 'จอสำหรับการสอน 1 จอ, เก้าอี้ 20 ตัว, คอมพิวเตอร์ 20 เครื่อง, ไมค์โครโฟน 1 ตัว, ลำโพง 2 ตัว',
    capacity: 20,
    building: building1,
    institution: Inst1,
    approver: approve1
  })
  const room2 = new Room({
    name: 'IF-3C02',
    desc: 'จอสำหรับการสอน 1 จอ, เก้าอี้ 20 ตัว, คอมพิวเตอร์ 20 เครื่อง, ไมค์โครโฟน 1 ตัว, ลำโพง 2 ตัว',
    capacity: 20,
    building: building1,
    institution: Inst1,
    approver: approve2
  })
  const room3 = new Room({
    name: 'SD-201',
    desc: 'จอสำหรับการสอน 1 จอ, เก้าอี้ 20 ตัว, ไมค์โครโฟน 1 ตัว, ลำโพง 2 ตัว',
    capacity: 20,
    building: building2,
    institution: Inst2,
    approver: approve2
  })

  await building1.save()
  await building2.save()
  await Inst1.save()
  await Inst2.save()
  await user.save()
  await admin.save()
  await localAdmin.save()
  await approver1.save()
  await approver2.save()
  await approver3.save()
  await approver4.save()
  await approve1.save()
  await approve2.save()
  await room1.save()
  await room2.save()
  await room3.save()
}
main().then(function () {
  console.log('Finish')
})
